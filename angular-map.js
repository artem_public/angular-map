'use strict';

angular.
module('angularMap', []);

angular.
module('angularMap').service('angularMapService', [
    function() {
        var vm = this;

        vm.toWkt = function(olGeometry) {
                var wktParser = new OpenLayers.Format.WKT();
                return wktParser.extractGeometry(olGeometry);
            },

            vm.fromWkt = function(wkt) {
                var wktParser = new OpenLayers.Format.WKT();
                return wktParser.read(wkt).geometry;
            },

            vm.convertGeometrySrid = function(olGeometry, fromSrid, toSrid) {
                return olGeometry.transform(new OpenLayers.Projection(fromSrid), new OpenLayers.Projection(toSrid));
            },

            vm.getBlueStyleMap = function() {

                // style the sketch fancy
                var sketchSymbolizers = {
                    "Point": {
                        //pointRadius: 4,
                        //graphicName: "square",
                        externalGraphic: vm.getPin(),
                        graphicWidth: 32,
                        graphicHeight: 32,
                        fillColor: "#2196F3",
                        fillOpacity: 1,
                        strokeWidth: 1,
                        strokeOpacity: 1,
                        strokeColor: "white"
                    },
                    "Line": {
                        strokeWidth: 3,
                        strokeOpacity: 1,
                        strokeColor: "#2196F3"
                            /*,
                                                 strokeDashstyle: "dash"*/
                    },
                    "Polygon": {
                        strokeWidth: 2,
                        strokeOpacity: 1,
                        strokeColor: "#2196F3",
                        fillColor: "white",
                        fillOpacity: 0.3
                    }
                };
                var style = new OpenLayers.Style();
                style.addRules([
                    new OpenLayers.Rule({ symbolizer: sketchSymbolizers })
                ]);
                var styleMap = new OpenLayers.StyleMap({ "default": style });

                return styleMap;

            },

            vm.getPin = function() {
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAA2lJREFUWIXtVltMU0kY/k7nYEvRgiGobVE0anTTjcSA4iVSixqqKYTV1N1H13uMwbjBzeoLEo0xvug+mOxuoD4Yo/GFiBpvVQzW1Xo5ILpeklINrdp6AVwrWNpzfh+kUFBwEJAXv2SSmTnfN/83c/75M8AwQ+Al2u12FgwGC2VZnjCAeC2CIFS5XK7m2ITIo7LZbFpRFK+Ulu6cmZyczG26J1QgOByHfK2trRZJkhq4hQsWmLe7r7spGpUH3BobfVRQUFDVZYoDanXCdIPR+BV7/hQGgwGMsfGxcbdfYDabNYyx1J4iRVGSmpqakJCQ0OvCosiQkjKazwVR58Y7DZhWla59xYQtClN/jEJdfFUkot929F+IrIMe+0ZdfabWItuoxY5fbRAE/jQRAWB+wco5gdHGPxtmr9OCACgdLa7/Xy/z8f1bD85hbOUlrF2+iNuACgDCmpS8F1MtWm5VLwjqF+Jegx+CgD7bJwaYElWrou0DjQ8VyYi0hfql4aoD/Qd/DnBdw6HEdwPDbmBIkrAfdWj4T+C7gW9uIBAIICrLvth4yCthOByG1+tFpL0dEARUlJc/9fv9W7sZIBV7qwiDcxiaUSmdfZ/P927DmtU32sLhCwoQkWW5JTExsbKuru51NwPi25eV+vun/3g/W58aGaFDf2q5AAIRIBBhkr8Ki1f8AEEAQqEQNm7ccPOs05nXt74DM1fvyI4otGtE65uxiqhtD6VOHtmQtd5EpOr1DZD2+GLjxCcnUqdNmZSkSdJh2fxM/GTJBgCUlJR4JEmyVldX9/n47MyBWseeWwCWxsb5+fkmzf+BI/cW7sz8nHBc4/nA9LsH9yltLdKMWenHt/2+Jj32zel0vnO73ftdLhf/y/dzmFf4S6Fh++mXqCBCORH+IcJfRCP3euRZyzcdjPFyc3P31NfXR4iIWpqbyWq1nhlQ4Hhk/bzZoT3g62Ygc+Vvd6xWqzqeV1RUdIeIqLi4+JHFYsngXZ99iWDS6y5rQgFbMCNvDAhIf3T8uf7Z1fUXz5z0xPPS0tLu19XW5t+WpN01NTXVg2bA6/WGJ2cYn8pMuwSkqCc+OFZx7cThv3vycnJy/N6HD08l6XQ1Ho9H5jXAfd+y7JsdTHk/d+mPRlNZWZnCqxs0mM1mzWK7PfmbBx5qfADvsJOY/QaTIwAAAABJRU5ErkJggg==';
            }

    }

]);

angular.
module('angularMap').
directive('mapPointChooser', ['$compile', '$timeout', 'angularMapService', function($compile, $timeout, angularMapService) {
    return {
        restrict: "A",
        template: '<div style="width:100%; height: 100%"></div>',
        scope: {
            pointModel: '='
        },

        link: function($scope, element, attributes, pointModel) {

            $timeout(function() {

                var options = {
                    displayProjection: new OpenLayers.Projection("EPSG:4326"),
                    projection: "EPSG:4326",
                    units: "degrees",
                    transitionEffect: null,
                    zoomMethod: null,
                    zoomDuration: 10,
                    controls: [
                        new OpenLayers.Control.Navigation(),
                        new OpenLayers.Control.Zoom()
                    ]
                };

                var map = new OpenLayers.Map(element[0].firstChild, options);

                // init layers
                var baseLayer = attributes.baseLayerSource ?
                    new OpenLayers.Layer.OSM('Custom Base Map', attributes.baseLayerSource) :
                    new OpenLayers.Layer.OSM("Simple OSM Base Map");

                var drawingsLayerSM = angularMapService.getBlueStyleMap();
                var drawingsLayer = new OpenLayers.Layer.Vector('Drawings Layer', {
                    styleMap: drawingsLayerSM
                });
                map.addLayers([baseLayer, drawingsLayer]);

                var centerPoint = attributes.initialCenter ?
                    new OpenLayers.Geometry.Point(attributes.initialCenter.split(',')[0], attributes.initialCenter.split(',')[1]) :
                    new OpenLayers.Geometry.Point(44.7807474, 41.7138468);

                var centerPoint900913, zoom;

                if (attributes.pinLocation) {
                    
                    var olGeom = angularMapService.fromWkt(attributes.pinLocation);

                    centerPoint900913 = angularMapService.convertGeometrySrid(olGeom, 'EPSG:4326', 'EPSG:900913');

                    var feature = new OpenLayers.Feature.Vector(olGeom, []);
                    drawingsLayer.addFeatures([feature]);

                    zoom = 15;

                } else {
                    centerPoint900913 = angularMapService.convertGeometrySrid(centerPoint, 'EPSG:4326', 'EPSG:900913');
                    zoom = attributes.initialZoom ? attributes.initialZoom : 14;

                    var drawPointControl = new OpenLayers.Control.DrawFeature(
                        drawingsLayer,
                        OpenLayers.Handler.Point, {
                            multi: false,
                            featureAdded: function(feature) {

                                drawingsLayer.removeAllFeatures();

                                drawingsLayer.addFeatures(feature);

                                var olGeometry4326 = angularMapService.convertGeometrySrid(angular.copy(feature.geometry), 'EPSG:900913', 'EPSG:4326');

                                $scope.pointModel = angularMapService.toWkt(olGeometry4326);
                                $scope.$apply();

                            }
                        });

                    map.addControls([drawPointControl]);

                    drawPointControl.activate();

                }

                map.setCenter(new OpenLayers.LonLat(centerPoint900913.x, centerPoint900913.y), zoom);

            }, 500);



        }
    }
}]);

/*
Usage example:
*/

/*
// usage as form field for choosing coordinates
<div style="width: 700px; height: 500px; margin: 0px auto;"
    map-point-chooser
    point-model="choosedCoordinates"
    initial-zoom="14"
    initial-center="44.7807474,41.7138468"
    base-layer-source="http://mapstiles.tbilisi.gov.ge/tbilisimap-tileserver/tilesproxy?x=${x}&y=${y}&z=${z}&l=msda-satellite">
</div>

// usage as choosed pin view
<div style="width: 500px; height: 300px; margin: 0px auto;"
    map-point-chooser
    pin-location="POINT(44.78984545297821 41.72320043691731)"
    base-layer-source="http://mapstiles.tbilisi.gov.ge/tbilisimap-tileserver/tilesproxy?x=${x}&y=${y}&z=${z}&l=msda-satellite">
</div>

*/
