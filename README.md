/*
Usage example:
*/

// usage as form field for choosing coordinates
<div style="width: 700px; height: 500px; margin: 0px auto;"
    map-point-chooser
    point-model="choosedCoordinates"
    initial-zoom="14"
    initial-center="44.7807474,41.7138468"
    base-layer-source="http://mapstiles.tbilisi.gov.ge/tbilisimap-tileserver/tilesproxy?x=${x}&y=${y}&z=${z}&l=msda-satellite">
</div>

// usage as choosed pin view
<div style="width: 500px; height: 300px; margin: 0px auto;"
    map-point-chooser
    pin-location="POINT(44.78984545297821 41.72320043691731)"
    base-layer-source="http://mapstiles.tbilisi.gov.ge/tbilisimap-tileserver/tilesproxy?x=${x}&y=${y}&z=${z}&l=msda-satellite">
</div>